#ifndef BMD_H
#define BMD_H

#include <pthread.h>
#include "DeckLinkAPI.h"

#include "PreviewWindow.h"

typedef bool frameArrivedCallback(long, long, long, BMDPixelFormat, void*);
typedef bool callback0();
typedef bool callback2(unsigned long, unsigned long);
static pthread_mutex_t	g_sleepMutex;
static pthread_cond_t	g_sleepCond;

class DeckLinkCaptureDelegate;

class BMD
{
    public:
        BMD(int, int, int, long bufferSize=1);
        virtual ~BMD();
        // arg: bmdVideoConnectionSDI or bmdVideoConnectionHDMI
        HRESULT switchInputConnection(BMDVideoConnection);
        // arg: 0- infinite loop else until number of frames is grabbed
        HRESULT startLoop(unsigned long);
        HRESULT stopLoop();
        // method allocates memory for the given pointer
        // you need to manage to free the memory after you finished
        HRESULT getFrameFromBuffer(char*);
        void setFrameArrivedCallback(frameArrivedCallback);
        void setStartLoopCallback(callback0);
        void setStopLoopCallback(callback2);
        unsigned long getFrameLimit();

        static void freeBuffer(char*);
        static void listSupportedPixelFormats();
        static int listDisplayModes();

        void startPreview();
        void stopPreview();
        void updatePreviewBuffer(char*);

        IDeckLink* deckLink;
        IDeckLinkInput* deckLinkInput;
        BMDPixelFormat pixelFormat;

        frameArrivedCallback* frameCallback;
        callback0* startLoopCallback;
        callback2* stopLoopCallback;
    protected:
    private:
        IDeckLinkInput* getDeckLinkInput();
        IDeckLinkDisplayMode* getDeckLinkDisplayMode(IDeckLinkInput*, int);

        DeckLinkCaptureDelegate* delegate;
        IDeckLinkDisplayMode* deckLinkDisplayMode;
        HRESULT result;

        unsigned long frameLimit;

        // SFML
        PreviewWindow* prevWin;
};

// interface
// prefixes:
// BMD - regular methods, BMDS - static methods
extern "C" {
    void* BMD_New(int dev_id=0, int=0, int=0, long bufferSize=1);
    void BMD_Delete(void*);
    HRESULT BMD_startLoop(void*, unsigned long);
    HRESULT BMD_stopLoop(void*);
    HRESULT BMD_switchInputConnection(void*, BMDVideoConnection);
    HRESULT BMD_getFrameFromBuffer(void*, char*);
    void BMD_setFrameArrivedCallback(void*, frameArrivedCallback);
    void BMD_setStartLoopCallback(void*, callback0);
    void BMD_setStopLoopCallback(void*, callback2);
    void BMDS_freeBuffer(char*);
    int BMDS_listDisplayModes();
    void BMDS_listSupportedPixelFormats();
}

#endif // BMD_H
