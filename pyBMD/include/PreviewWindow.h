#ifndef PREVIEWWINDOW_H
#define PREVIEWWINDOW_H

#include "SFML/Graphics.hpp"

class PreviewWindow
{
    public:
        PreviewWindow();
        virtual ~PreviewWindow();

        void updateTex(char*);
    protected:
    private:
        void render();

        sf::RenderWindow* window;
        sf::Texture* texture;
        sf::Thread m_thread;

        std::vector<char* (*)(char*)> processFuncs;
};

#endif // PREVIEWWINDOW_H
