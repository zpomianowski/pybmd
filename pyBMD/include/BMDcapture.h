#ifndef BMDCAPTURE_H_INCLUDED
#define BMDCAPTURE_H_INCLUDED

#include <pthread.h>
#include "BMD.h"
#include "CircularBuffer.h"
#include "DeckLinkAPI.h"

class DeckLinkCaptureDelegate : public IDeckLinkInputCallback
{
public:
	DeckLinkCaptureDelegate(BMD*, int);
	virtual ~DeckLinkCaptureDelegate();

	virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID iid, LPVOID *ppv) { return E_NOINTERFACE; }
	virtual ULONG STDMETHODCALLTYPE AddRef(void);
	virtual ULONG STDMETHODCALLTYPE  Release(void);
	virtual HRESULT STDMETHODCALLTYPE VideoInputFormatChanged(BMDVideoInputFormatChangedEvents, IDeckLinkDisplayMode*, BMDDetectedVideoInputFormatFlags);
	virtual HRESULT STDMETHODCALLTYPE VideoInputFrameArrived(IDeckLinkVideoInputFrame*, IDeckLinkAudioInputPacket*);

	unsigned long getValidFramesNb();
	unsigned long getInvalidFramesNb();
	void runThread(void (*func)());

	void addProcessFunc(void (BMD::*)(char*));

	HRESULT getFrameFromBuffer(char* data);
private:
	BMD*                parent;
	ULONG				m_refCount;
	pthread_mutex_t		m_mutex;
	unsigned long  	    frameCount_Valid;
	unsigned long  	    frameCount_Invalid;

	CircularBuffer*     buffer;
	int                 bufferCapacity;
	long                frameWidth;
	long                frameHeight;
	long                bytesInRow;

	std::vector<void (BMD::*)(char*)> processFuncs;
};

#endif // BMDCAPTURE_H_INCLUDED
