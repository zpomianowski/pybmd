#include <stdio.h>
#include <stdexcept>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <csignal>

#include "DeckLinkAPI.h"
#include "BMDcapture.h"
#include "CircularBuffer.h"

DeckLinkCaptureDelegate::DeckLinkCaptureDelegate(BMD* parent, int bufferCapacity)
    : m_refCount(0)
{
    this->parent = parent;
    this->frameCount_Invalid = 0;
    this->frameCount_Valid = 0;
    this->buffer = NULL;
    this->bufferCapacity = bufferCapacity;
    this->frameWidth = 0;
    this->frameHeight = 0;
    this->bytesInRow = 0;
	pthread_mutex_init(&m_mutex, NULL);
}

DeckLinkCaptureDelegate::~DeckLinkCaptureDelegate()
{
	pthread_mutex_destroy(&m_mutex);
}

ULONG DeckLinkCaptureDelegate::AddRef(void)
{
	pthread_mutex_lock(&m_mutex);
		m_refCount++;
	pthread_mutex_unlock(&m_mutex);

	return (ULONG)m_refCount;
}

ULONG DeckLinkCaptureDelegate::Release(void)
{
	//pthread_mutex_lock(&m_mutex);
	//--m_refCount;
	//pthread_mutex_unlock(&m_mutex);

	if (m_refCount == 0)
	{
		delete this;
		return 0;
	}

	return (ULONG)m_refCount;
}

HRESULT DeckLinkCaptureDelegate::VideoInputFrameArrived(IDeckLinkVideoInputFrame* videoFrame, IDeckLinkAudioInputPacket* audioFrame)
{
	IDeckLinkVideoFrame*				rightEyeFrame = NULL;
	IDeckLinkVideoFrame3DExtensions*	threeDExtensions = NULL;
	void*								frameBytes;
	void*								audioFrameBytes;
    unsigned long                       frameLimit;

    frameLimit = this->parent->getFrameLimit();
	if ((frameLimit == frameCount_Valid + frameCount_Invalid) && frameLimit != 0) {
        this->parent->stopLoop();
        return S_OK;
    }

	// Handle Video Frame
	if (videoFrame)
	{
		// If 3D mode is enabled we retreive the 3D extensions interface which gives.
		// us access to the right eye frame by calling GetFrameForRightEye() .
		if ( (videoFrame->QueryInterface(IID_IDeckLinkVideoFrame3DExtensions, (void **) &threeDExtensions) != S_OK) ||
			(threeDExtensions->GetFrameForRightEye(&rightEyeFrame) != S_OK))
		{
			rightEyeFrame = NULL;
		}

		if (threeDExtensions)
			threeDExtensions->Release();

		if (videoFrame->GetFlags() & bmdFrameHasNoInputSource)
		{
			++frameCount_Invalid;
		}
		else
		{
            ++frameCount_Valid;
            HRESULT result = videoFrame->GetBytes(&frameBytes);
            long frame_width = videoFrame->GetWidth();
            long frame_height = videoFrame->GetHeight();
            long bytes_in_row = videoFrame->GetRowBytes();
            if (result == S_OK) {
                buffer->write((const char*) frameBytes, frame_height * bytes_in_row);
            }

            if (this->parent->frameCallback)
                this->parent->frameCallback(
                    frame_width,
                    frame_height,
                    bytes_in_row,
                    videoFrame->GetPixelFormat(),
                    frameBytes);

            // processing funcs
            /*char* procbuf = (char*) malloc(frame_height * bytes_in_row * sizeof(char));
            memcpy(procbuf, frameBytes, frame_height * bytes_in_row);
            for (auto func : processFuncs) {
                (parent->*func)(procbuf);
            }*/
		}

		if (rightEyeFrame)
			rightEyeFrame->Release();
}

	// Handle Audio Frame
	if (audioFrame)
	{
	}

	return S_OK;
}

HRESULT DeckLinkCaptureDelegate::VideoInputFormatChanged(
    BMDVideoInputFormatChangedEvents events,
    IDeckLinkDisplayMode *mode,
    BMDDetectedVideoInputFormatFlags)
{
	HRESULT	result;
	BMD* parent = this->parent;
	char* displayModeName = NULL;

	if (!(events & bmdVideoInputDisplayModeChanged))
		return S_OK;

	mode->GetName((const char**)&displayModeName);
	printf("Video format changed to %s\n", displayModeName);

	if (buffer) buffer->~CircularBuffer();
	int bytes_per_pixel = NULL;
	switch(parent->pixelFormat) {
        case bmdFormat8BitYUV:
            bytes_per_pixel = 3;
        break;
	}
	if (!bytes_per_pixel)
	{
        fprintf(stdout, "ERR: Bytes per pixel unknown. Need to establish data type for VideoFormat.\n");
        throw std::bad_exception();
    }
    frameHeight = mode->GetHeight();
    frameWidth = mode->GetWidth();
    bytesInRow = bytes_per_pixel * frameWidth;
	buffer = new CircularBuffer(
        mode->GetWidth() * mode->GetHeight() * bytes_per_pixel * sizeof(char) * this->bufferCapacity);

	if (displayModeName)
		free(displayModeName);

	if (parent->deckLinkInput)
	{
		parent->deckLinkInput->StopStreams();

		result = this->parent->deckLinkInput->EnableVideoInput(mode->GetDisplayMode(),
                                                       parent->pixelFormat,
                                                       bmdVideoInputEnableFormatDetection);
		if (result != S_OK)
		{
			fprintf(stderr, "Failed to switch video mode\n");
			goto bail;
		}

		parent->deckLinkInput->StartStreams();
	}

bail:
	return S_OK;
}

unsigned long DeckLinkCaptureDelegate::getValidFramesNb()
{
    return this->frameCount_Valid;
}

unsigned long DeckLinkCaptureDelegate::getInvalidFramesNb()
{
    return this->frameCount_Invalid;
}

HRESULT DeckLinkCaptureDelegate::getFrameFromBuffer(char* data)
{
    //data = (char*) malloc(frameHeight * bytesInRow * sizeof(char));
    size_t dataSize = buffer->read(data, frameHeight * bytesInRow);
    if (dataSize > 0) return S_OK;
    return E_FAIL;
}

void DeckLinkCaptureDelegate::addProcessFunc(void (BMD::*func)(char*))
{
    processFuncs.push_back(func);
}

