#include "PreviewWindow.h"
#include <stdio.h>
#include "SFML/Graphics.hpp"

using namespace std;

PreviewWindow::PreviewWindow() :
    m_thread(&PreviewWindow::render, this)
{
    window = new sf::RenderWindow(sf::VideoMode(800, 600), "Preview window");
    window->setActive(false);
    window->setFramerateLimit(60);
    m_thread.launch();
}

PreviewWindow::~PreviewWindow()
{
    window->~Window();
}

void PreviewWindow::updateTex(char* buffer)
{

}

void PreviewWindow::render()
{
    sf::Texture texture;
    texture.setSmooth(true);
    texture.loadFromFile("test.png");

    sf::Sprite sprite;
    sprite.setTexture(texture);
   // The main loop - ends as soon as the window is closed
    while (window->isOpen())
    {
       // Event processing
       sf::Event event;
       while (window->pollEvent(event))
       {
           // Request for closing the window
           if (event.type == sf::Event::Closed)
               window->close();
       }
       // Clear the whole window before rendering a new frame
       window->clear();
       // Draw some graphical entities
       window->draw(sprite);
       // End the current frame and display its contents on screen
       window->display();
    }
}
