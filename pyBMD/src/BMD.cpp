#include <stdio.h>
#include <stdlib.h>
#include <stdexcept>
#include <pthread.h>
#include <pthread.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#include "BMD.h"
#include "BMDcapture.h"
#include "DeckLinkAPI.h"

#define EXECUTE(_CLASS, _METHOD) \
    return ((_CLASS*) Handle)->_METHOD


// List of known pixel formats and their matching display names
const BMDPixelFormat gKnownPixelFormats[] = {
    bmdFormat8BitYUV, bmdFormat10BitYUV, bmdFormat8BitARGB,
    bmdFormat8BitBGRA, bmdFormat10BitRGB, bmdFormat12BitRGB,
    bmdFormat12BitRGBLE, bmdFormat10BitRGBXLE, bmdFormat10BitRGBX, 0 };

const char* gKnownPixelFormatNames[] = {
    "8-bit YUV", "10-bit YUV", "8-bit ARGB",
    "8-bit BGRA", "10-bit RGB", "12-bit RGB",
    "12-bit RGBLE", "10-bit RGBXLE", "10-bit RGBX", NULL };

void	print_attributes (IDeckLink* deckLink);
void	print_output_modes (IDeckLink* deckLink);
void	print_input_modes (IDeckLink* deckLink);
void	print_capabilities (IDeckLink* deckLink);

// *******************************************************
// src
// *******************************************************
BMD::BMD(int deviceId=0, int pixelFormatId=0, int displayModeId=0, long bufferSize)
{
    pthread_mutex_init(&g_sleepMutex, NULL);
    int idx = deviceId;
    deckLink = NULL;
    this->pixelFormat = gKnownPixelFormats[pixelFormatId];
    this->frameCallback = NULL;
    this->startLoopCallback = NULL;
    this->stopLoopCallback = NULL;

    IDeckLinkIterator* deckLinkIterator = CreateDeckLinkIteratorInstance();
    if (deckLinkIterator == NULL)
    {
        fprintf(stderr, "ERR: Cannot find any DeckLink device\n");
        result = E_FAIL;
        throw std::bad_exception();
    }
    while (result = deckLinkIterator->Next(&deckLink) == S_OK)
    {
        if (idx == 0)
			break;
		--idx;
		deckLink->Release();
    }
    deckLinkIterator->Release();

    /*if (result != S_OK || deckLink == NULL)
    {
        result = E_FAIL;
        fprintf(stderr, "ERR: Cannot iterate this device\n");
        throw std::bad_exception();
    }*/
    deckLinkInput = this->getDeckLinkInput();
    deckLinkDisplayMode = this->getDeckLinkDisplayMode(deckLinkInput, displayModeId);

    this->switchInputConnection(bmdVideoConnectionHDMI);

    // set auto format detection
    IDeckLinkAttributes* deckLinkAttributes;
    result = deckLink->QueryInterface(IID_IDeckLinkAttributes, (void**) &deckLinkAttributes);
    if (result == S_OK)
    {
        bool formatDetectionSupported = false;
        result = deckLinkAttributes->GetFlag(BMDDeckLinkSupportsInputFormatDetection, &formatDetectionSupported);
        if (result != S_OK || !formatDetectionSupported)
        {
            fprintf(stdout, "ERR: Format detection not supported on this device\n");
            throw std::bad_exception();
        }
    }

    // set delegate
    delegate = new DeckLinkCaptureDelegate(this, bufferSize);
	deckLinkInput->SetCallback(delegate);

    // enable video input
    result = deckLinkInput->EnableVideoInput(deckLinkDisplayMode->GetDisplayMode(),
                                             this->pixelFormat,
                                             bmdVideoInputEnableFormatDetection);
    if (result != S_OK) this->~BMD();
}

BMD::~BMD()
{
    pthread_mutex_destroy(&g_sleepMutex);
    if (deckLink) {
        deckLink->Release();
    }
    if (deckLinkInput)
    {
        deckLinkInput->StopStreams();
        deckLinkInput->DisableAudioInput();
        deckLinkInput->DisableVideoInput();
        deckLinkInput->Release();
    }
    if (deckLinkDisplayMode) {
        deckLinkDisplayMode->Release();
    }
    if (delegate) {
        delegate->Release();
    }
}

IDeckLinkInput* BMD::getDeckLinkInput()
{
    IDeckLinkInput* deckLinkInput;
    result = deckLink->QueryInterface(IID_IDeckLinkInput, (void**)&deckLinkInput);
	if (result == S_OK)
        return deckLinkInput;
	else return NULL;
}

IDeckLinkDisplayMode* BMD::getDeckLinkDisplayMode(IDeckLinkInput* deckLinkInput, int mode_id)
{
    IDeckLinkDisplayMode*			displayMode = NULL;
	IDeckLinkDisplayModeIterator*	displayModeIterator = NULL;
	int								i = mode_id;

	char* displayModeName;

	result = deckLinkInput->GetDisplayModeIterator(&displayModeIterator);
	if (result != S_OK)
		goto bail;

	while ((result = displayModeIterator->Next(&displayMode)) == S_OK)
	{
        result = displayMode->GetName((const char **)&displayModeName);
		if (i == 0)
			break;
		--i;
		displayMode->Release();
	}

	if (result != S_OK)
		goto bail;

bail:
	if (displayModeIterator)
		displayModeIterator->Release();

	return displayMode;
}

void BMD::listSupportedPixelFormats()
{
    int t_iters = 0;
    fprintf(stdout, "\nSupported pixel formats:\n");
	while ((gKnownPixelFormats[t_iters] != 0) && (gKnownPixelFormatNames[t_iters] != NULL)) {
        fprintf(stdout, "%d\t\t:  %-20s\n", t_iters, gKnownPixelFormatNames[t_iters]);
        ++t_iters;
	}
}

HRESULT BMD::switchInputConnection(BMDVideoConnection connection)
{
	IDeckLinkConfiguration*		deckLinkConfiguration = NULL;

	HRESULT						result;

    deckLinkInput->StopStreams();
	result = deckLink->QueryInterface(IID_IDeckLinkConfiguration , (void**)&deckLinkConfiguration);
	if (result != S_OK)
	{
		fprintf(stderr, "Could not obtain the IID_IDeckLinkConfiguration interface - result = %08x\n", result);
		goto bail;
	}

	result = deckLinkConfiguration->SetInt(bmdDeckLinkConfigVideoInputConnection, connection);

	printf("\nConnection Switch: ");

	if (result == S_OK)printf("Success");
	else if(result == E_FAIL)printf("Failed");
	else if(result == E_INVALIDARG)printf("Invalid");
	else if(result == E_NOTIMPL)printf("Not Supported");

	result = deckLinkConfiguration->WriteConfigurationToPreferences();

	printf("\nConfig Save: ");

	if (result == S_OK)printf("Success");
	else if(result == E_FAIL)printf("Failed");
	else if(result == E_ACCESSDENIED)printf("Access Denied");

	printf("\n\n");
	return S_OK;

bail:
	if (deckLinkConfiguration != NULL)
		deckLinkConfiguration->Release();
    return E_FAIL;
}

unsigned long BMD::getFrameLimit()
{
    return this->frameLimit;
}

HRESULT BMD::getFrameFromBuffer(char* data)
{
    return delegate->getFrameFromBuffer(data);
}

void BMD::freeBuffer(char* ptr)
{
    free(ptr);
}

HRESULT BMD::startLoop(unsigned long frameLimit = 0)
{


    this->frameLimit = frameLimit;
    deckLinkInput->StartStreams();
    if (startLoopCallback) startLoopCallback();

    pthread_mutex_lock(&g_sleepMutex);
    pthread_cond_wait(&g_sleepCond, &g_sleepMutex);
    pthread_mutex_unlock(&g_sleepMutex);

    if (stopLoopCallback)
        stopLoopCallback(
            delegate->getInvalidFramesNb(),
            delegate->getValidFramesNb());
    deckLinkInput->StopStreams();
    return S_OK;
}

HRESULT BMD::stopLoop()
{
    pthread_cond_signal(&g_sleepCond);
    return S_OK;
}

void BMD::setFrameArrivedCallback(frameArrivedCallback F)
{
    this->frameCallback = F;
}

void BMD::setStartLoopCallback(callback0 F)
{
    this->startLoopCallback = F;
}

void BMD::setStopLoopCallback(callback2 F)
{
    this->stopLoopCallback = F;
}

int BMD::listDisplayModes()
{
    HRESULT result;
    int numDevices = 0;
    IDeckLinkIterator* deckLinkIterator = NULL;
    IDeckLink* deckLink = NULL;

    deckLinkIterator = CreateDeckLinkIteratorInstance();
    if (deckLinkIterator == NULL)
    {
        fprintf(stderr, "ERR: Cannot find any DeckLink device\n");
        return 0;
    }
    while (deckLinkIterator->Next(&deckLink) == S_OK)
    {
        ++numDevices;
        char* deviceNameString = NULL;
        result = deckLink->GetModelName((const char **) &deviceNameString);
        if (result == S_OK)
        {
            fprintf(stderr, "Found device: %s\n", deviceNameString);
            free(deviceNameString);
        }
        print_attributes(deckLink);

		// ** List the video output display modes supported by the card
		print_output_modes(deckLink);

		// ** List the video input display modes supported by the card
		print_input_modes(deckLink);

		// ** List the input and output capabilities of the card
		print_capabilities(deckLink);

		// Release the IDeckLink instance when we've finished with
		// it to prevent leaks
        deckLink->Release();
    }
    deckLinkIterator->Release();
    return numDevices;
}

void BMD::startPreview()
{
    prevWin = new PreviewWindow();
    delegate->addProcessFunc(&BMD::updatePreviewBuffer);
}

void BMD::stopPreview()
{
    prevWin->~PreviewWindow();
}

void BMD::updatePreviewBuffer(char* buffer)
{
    if (!prevWin)
    {
        free(buffer);
        return;
    }
    prevWin->updateTex(buffer);
    free(buffer);
}

// *******************************************************
// expose to ctypes - in c style
// *******************************************************
void* BMD_New(int deviceId, int pixelFormat, int displayMode, long bufferSize)
{
    return new BMD(deviceId, pixelFormat, displayMode, bufferSize);
}

void BMD_Delete(void* Handle)
{
    EXECUTE(BMD, ~BMD());
}

HRESULT BMD_startLoop(void* Handle, unsigned long limit)
{
    EXECUTE(BMD, startLoop(limit));
}

HRESULT BMD_stopLoop(void* Handle)
{
    EXECUTE(BMD, stopLoop());
}

HRESULT BMD_getFrameFromBuffer(void* Handle, char* data)
{
    EXECUTE(BMD, getFrameFromBuffer(data));
}

void BMD_setFrameArrivedCallback(void* Handle, frameArrivedCallback F)
{
    EXECUTE(BMD, setFrameArrivedCallback(F));
}

void BMD_setStartLoopCallback(void* Handle, callback0 F)
{
    EXECUTE(BMD, setStartLoopCallback(F));
}
void BMD_setStopLoopCallback(void* Handle, callback2 F)
{
    EXECUTE(BMD, setStopLoopCallback(F));
}

HRESULT BMD_switchInputConnection(void* Handle, BMDVideoConnection connection)
{
    EXECUTE(BMD, switchInputConnection(connection));
}

void BMDS_freeBuffer(char* ptr)
{
    BMD::freeBuffer(ptr);
}

void BMDS_listSupportedPixelFormats()
{
    return BMD::listSupportedPixelFormats();
}

int BMDS_listDisplayModes()
{
    return BMD::listDisplayModes();
}


// *******************************************************
// utils
// *******************************************************
void	print_attributes (IDeckLink* deckLink)
{
	IDeckLinkAttributes*				deckLinkAttributes = NULL;
	bool								supported;
	int64_t								value;
	char *								serialPortName = NULL;
	HRESULT								result;

	// Query the DeckLink for its attributes interface
	result = deckLink->QueryInterface(IID_IDeckLinkAttributes, (void**)&deckLinkAttributes);
	if (result != S_OK)
	{
		fprintf(stderr, "Could not obtain the IDeckLinkAttributes interface - result = %08x\n", result);
		goto bail;
	}

	// List attributes and their value
	printf("Attribute list:\n");

	result = deckLinkAttributes->GetFlag(BMDDeckLinkHasSerialPort, &supported);
	if (result == S_OK)
	{
		printf(" %-40s %s\n", "Serial port present ?", (supported == true) ? "Yes" : "No");

		if (supported)
		{
			result = deckLinkAttributes->GetString(BMDDeckLinkSerialPortDeviceName, (const char **) &serialPortName);
			if (result == S_OK)
			{
				printf(" %-40s %s\n", "Serial port name: ", serialPortName);
				free(serialPortName);

			}
			else
			{
				fprintf(stderr, "Could not query the serial port presence attribute- result = %08x\n", result);
			}
		}

	}
	else
	{
		fprintf(stderr, "Could not query the serial port presence attribute- result = %08x\n", result);
	}

	result = deckLinkAttributes->GetInt(BMDDeckLinkPersistentID, &value);
	if (result == S_OK)
	{
		printf(" %-40s %llx\n", "Device Persistent ID:",  value);
	}
	else
	{
		printf(" %-40s %s\n", "Device Persistent ID:",  "Not Supported on this device");
	}

	result = deckLinkAttributes->GetInt(BMDDeckLinkTopologicalID, &value);
	if (result == S_OK)
	{
		printf(" %-40s %llx\n", "Device Topological ID:",  value);
	}
	else
	{
		printf(" %-40s %s\n", "Device Topological ID:",  "Not Supported on this device");
	}

    result = deckLinkAttributes->GetInt(BMDDeckLinkNumberOfSubDevices, &value);
    if (result == S_OK)
    {
        printf(" %-40s %" PRId64 "\n", "Number of sub-devices:",  value);
        if (value != 0)
        {
            result = deckLinkAttributes->GetInt(BMDDeckLinkSubDeviceIndex, &value);
            if (result == S_OK)
            {
                printf(" %-40s %" PRId64 "\n", "Sub-device index:",  value);
            }
            else
            {
                fprintf(stderr, "Could not query the sub-device index attribute- result = %08x\n", result);
            }
        }
    }
    else
    {
        fprintf(stderr, "Could not query the number of sub-device attribute- result = %08x\n", result);
    }

	result = deckLinkAttributes->GetInt(BMDDeckLinkMaximumAudioChannels, &value);
	if (result == S_OK)
	{
		printf(" %-40s %" PRId64 "\n", "Number of audio channels:",  value);
	}
	else
	{
		fprintf(stderr, "Could not query the number of supported audio channels attribute- result = %08x\n", result);
	}

	result = deckLinkAttributes->GetFlag(BMDDeckLinkSupportsInputFormatDetection, &supported);
	if (result == S_OK)
	{
		printf(" %-40s %s\n", "Input mode detection supported ?", (supported == true) ? "Yes" : "No");
	}
	else
	{
		fprintf(stderr, "Could not query the input mode detection attribute- result = %08x\n", result);
	}

	result = deckLinkAttributes->GetFlag(BMDDeckLinkSupportsFullDuplex, &supported);
	if (result == S_OK)
	{
		printf(" %-40s %s\n", "Full duplex operation supported ?", (supported == true) ? "Yes" : "No");
	}
	else
	{
		fprintf(stderr, "Could not query the full duplex operation supported attribute- result = %08x\n", result);
	}

	result = deckLinkAttributes->GetFlag(BMDDeckLinkSupportsInternalKeying, &supported);
	if (result == S_OK)
	{
		printf(" %-40s %s\n", "Internal keying supported ?", (supported == true) ? "Yes" : "No");
	}
	else
	{
		fprintf(stderr, "Could not query the internal keying attribute- result = %08x\n", result);
	}

	result = deckLinkAttributes->GetFlag(BMDDeckLinkSupportsExternalKeying, &supported);
	if (result == S_OK)
	{
		printf(" %-40s %s\n", "External keying supported ?", (supported == true) ? "Yes" : "No");
	}
	else
	{
		fprintf(stderr, "Could not query the external keying attribute- result = %08x\n", result);
	}

	result = deckLinkAttributes->GetFlag(BMDDeckLinkSupportsHDKeying, &supported);
	if (result == S_OK)
	{
		printf(" %-40s %s\n", "HD-mode keying supported ?", (supported == true) ? "Yes" : "No");
	}
	else
	{
		fprintf(stderr, "Could not query the HD-mode keying attribute- result = %08x\n", result);
	}

bail:
	printf("\n");
	if(deckLinkAttributes != NULL)
		deckLinkAttributes->Release();

}

void	print_output_modes (IDeckLink* deckLink)
{
	IDeckLinkOutput*					deckLinkOutput = NULL;
	IDeckLinkDisplayModeIterator*		displayModeIterator = NULL;
	IDeckLinkDisplayMode*				displayMode = NULL;
	HRESULT								result;

	// Query the DeckLink for its configuration interface
	result = deckLink->QueryInterface(IID_IDeckLinkOutput, (void**)&deckLinkOutput);
	if (result != S_OK)
	{
		fprintf(stderr, "Could not obtain the IDeckLinkOutput interface - result = %08x\n", result);
		goto bail;
	}

	// Obtain an IDeckLinkDisplayModeIterator to enumerate the display modes supported on output
	result = deckLinkOutput->GetDisplayModeIterator(&displayModeIterator);
	if (result != S_OK)
	{
		fprintf(stderr, "Could not obtain the video output display mode iterator - result = %08x\n", result);
		goto bail;
	}

	// List all supported output display modes
	printf("Supported video output display modes and pixel formats:\n");
	while (displayModeIterator->Next(&displayMode) == S_OK)
	{
		char *			displayModeString = NULL;

		result = displayMode->GetName((const char **) &displayModeString);
		if (result == S_OK)
		{
			char					modeName[64];
			int						modeWidth;
			int						modeHeight;
			BMDTimeValue			frameRateDuration;
			BMDTimeScale			frameRateScale;
			int						pixelFormatIndex = 0; // index into the gKnownPixelFormats / gKnownFormatNames arrays
			BMDDisplayModeSupport	displayModeSupport;


			// Obtain the display mode's properties
			modeWidth = displayMode->GetWidth();
			modeHeight = displayMode->GetHeight();
			displayMode->GetFrameRate(&frameRateDuration, &frameRateScale);
			printf(" %-20s \t %d x %d \t %7g FPS\t", displayModeString, modeWidth, modeHeight, (double)frameRateScale / (double)frameRateDuration);

			// Print the supported pixel formats for this display mode
			while ((gKnownPixelFormats[pixelFormatIndex] != 0) && (gKnownPixelFormatNames[pixelFormatIndex] != NULL))
			{
				if ((deckLinkOutput->DoesSupportVideoMode(displayMode->GetDisplayMode(), gKnownPixelFormats[pixelFormatIndex], bmdVideoOutputFlagDefault, &displayModeSupport, NULL) == S_OK)
						&& (displayModeSupport != bmdDisplayModeNotSupported))
				{
					printf("%s\t", gKnownPixelFormatNames[pixelFormatIndex]);
				}
				else
					printf("------\t\t");
				pixelFormatIndex++;
			}

			printf("\n");

			free(displayModeString);
		}

		// Release the IDeckLinkDisplayMode object to prevent a leak
		displayMode->Release();
	}

	printf("\n");

bail:
	// Ensure that the interfaces we obtained are released to prevent a memory leak
	if (displayModeIterator != NULL)
		displayModeIterator->Release();

	if (deckLinkOutput != NULL)
		deckLinkOutput->Release();
}


void	print_input_modes (IDeckLink* deckLink)
{
	IDeckLinkInput*					deckLinkInput = NULL;
	IDeckLinkDisplayModeIterator*		displayModeIterator = NULL;
	IDeckLinkDisplayMode*				displayMode = NULL;
	HRESULT								result;
	int t_iters = 0;

	// Query the DeckLink for its configuration interface
	result = deckLink->QueryInterface(IID_IDeckLinkInput, (void**)&deckLinkInput);
	if (result != S_OK)
	{
		fprintf(stderr, "Could not obtain the IDeckLinkInput interface - result = %08x\n", result);
		goto bail;
	}

	// Obtain an IDeckLinkDisplayModeIterator to enumerate the display modes supported on input
	result = deckLinkInput->GetDisplayModeIterator(&displayModeIterator);
	if (result != S_OK)
	{
		fprintf(stderr, "Could not obtain the video input display mode iterator - result = %08x\n", result);
		goto bail;
	}

	// List all supported input display modes
	printf("Supported video input display modes and pixel formats:\n");
	while (displayModeIterator->Next(&displayMode) == S_OK)
	{
		char *			displayModeString = NULL;

		result = displayMode->GetName((const char **) &displayModeString);
		if (result == S_OK)
		{
			char					modeName[64];
			int						modeWidth;
			int						modeHeight;
			BMDTimeValue			frameRateDuration;
			BMDTimeScale			frameRateScale;
			int						pixelFormatIndex = 0; // index into the gKnownPixelFormats / gKnownFormatNames arrays
			BMDDisplayModeSupport	displayModeSupport;


			// Obtain the display mode's properties
			modeWidth = displayMode->GetWidth();
			modeHeight = displayMode->GetHeight();
			displayMode->GetFrameRate(&frameRateDuration, &frameRateScale);
			printf("%d:\t\t%-20s \t %d x %d \t %7g FPS\t", t_iters, displayModeString, modeWidth, modeHeight, (double)frameRateScale / (double)frameRateDuration);
            ++t_iters;

			// Print the supported pixel formats for this display mode
			while ((gKnownPixelFormats[pixelFormatIndex] != 0) && (gKnownPixelFormatNames[pixelFormatIndex] != NULL))
			{
				if ((deckLinkInput->DoesSupportVideoMode(displayMode->GetDisplayMode(), gKnownPixelFormats[pixelFormatIndex], bmdVideoInputFlagDefault, &displayModeSupport, NULL) == S_OK)
						&& (displayModeSupport != bmdDisplayModeNotSupported))
				{
					printf("%s\t", gKnownPixelFormatNames[pixelFormatIndex]);
				}
				else
					printf("------\t\t");
				pixelFormatIndex++;
			}

			printf("\n");

			free(displayModeString);
		}

		// Release the IDeckLinkDisplayMode object to prevent a leak
		displayMode->Release();
	}

	printf("\n");

bail:
	// Ensure that the interfaces we obtained are released to prevent a memory leak
	if (displayModeIterator != NULL)
		displayModeIterator->Release();

	if (deckLinkInput != NULL)
		deckLinkInput->Release();
}


void	print_capabilities (IDeckLink* deckLink)
{
	IDeckLinkAttributes*		deckLinkAttributes = NULL;
	int64_t						ports;
	int							itemCount;
	HRESULT						result;

	// Query the DeckLink for its configuration interface
	result = deckLink->QueryInterface(IID_IDeckLinkAttributes, (void**)&deckLinkAttributes);
	if (result != S_OK)
	{
		fprintf(stderr, "Could not obtain the IDeckLinkAttributes interface - result = %08x\n", result);
		goto bail;
	}

	printf("Supported video output connections:\n  ");
	itemCount = 0;
	result = deckLinkAttributes->GetInt(BMDDeckLinkVideoOutputConnections, &ports);
	if (result == S_OK)
	{
		if (ports & bmdVideoConnectionSDI)
		{
			itemCount++;
			printf("SDI");
		}

		if (ports & bmdVideoConnectionHDMI)
		{
			if (itemCount++ > 0)
				printf(", ");
			printf("HDMI");
		}

		if (ports & bmdVideoConnectionOpticalSDI)
		{
			if (itemCount++ > 0)
				printf(", ");
			printf("Optical SDI");
		}

		if (ports & bmdVideoConnectionComponent)
		{
			if (itemCount++ > 0)
				printf(", ");
			printf("Component");
		}

		if (ports & bmdVideoConnectionComposite)
		{
			if (itemCount++ > 0)
				printf(", ");
			printf("Composite");
		}

		if (ports & bmdVideoConnectionSVideo)
		{
			if (itemCount++ > 0)
				printf(", ");
			printf("S-Video");
		}
	}
	else
	{
		fprintf(stderr, "Could not obtain the list of output ports - result = %08x\n", result);
		goto bail;
	}

	printf("\n\n");

	printf("Supported video input connections:\n  ");
	itemCount = 0;
	result = deckLinkAttributes->GetInt(BMDDeckLinkVideoInputConnections, &ports);
	if (result == S_OK)
	{
		if (ports & bmdVideoConnectionSDI)
		{
			itemCount++;
			printf("SDI");
		}

		if (ports & bmdVideoConnectionHDMI)
		{
			if (itemCount++ > 0)
				printf(", ");
			printf("HDMI");
		}

		if (ports & bmdVideoConnectionOpticalSDI)
		{
			if (itemCount++ > 0)
				printf(", ");
			printf("Optical SDI");
		}

		if (ports & bmdVideoConnectionComponent)
		{
			if (itemCount++ > 0)
				printf(", ");
			printf("Component");
		}

		if (ports & bmdVideoConnectionComposite)
		{
			if (itemCount++ > 0)
				printf(", ");
			printf("Composite");
		}

		if (ports & bmdVideoConnectionSVideo)
		{
			if (itemCount++ > 0)
				printf(", ");
			printf("S-Video");
		}
	}
	else
	{
		fprintf(stderr, "Could not obtain the list of input ports - result = %08x\n", result);
		goto bail;
	}
	printf("\n");

bail:
	if (deckLinkAttributes != NULL)
		deckLinkAttributes->Release();
}

