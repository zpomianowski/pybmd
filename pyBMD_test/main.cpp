#include <iostream>
#include <stdio.h>
#include <csignal>
#include <unistd.h>
#include "BMD.h"

using namespace std;

BMD* device;

static void sigfunc(int signum)
{
    if (signum == SIGINT || signum == SIGTERM)
        device->stopLoop();
}

// callback frameArrived - out of box test
bool callback_frame(long w, long h, long inrow, BMDPixelFormat format, void* frameBytes)
{
    fprintf(stdout, "WIDTH: %lupx\nHEIGHT: %lupx\nBYTES IN ROW: %lu\n\n",
            w, h, inrow);
    return false;
}

// signal callback test - out of box test
bool callback_start()
{
    fprintf(stdout, "Callback start test success!\n");
    return false;
}

bool callback_stop(unsigned long a, unsigned long b)
{
    fprintf(stdout, "Invalid frames: %lu, Valid frames: %lu\n", a, b);
    return false;
}

int main()
{
    signal(SIGINT, sigfunc);
    signal(SIGTERM, sigfunc);
    signal(SIGHUP, sigfunc);

    BMD::listDisplayModes();
    device = new BMD(0, 0, 0);
    device->setFrameArrivedCallback(callback_frame);
    device->setStartLoopCallback(callback_start);
    device->setStopLoopCallback(callback_stop);
    // x = 0 -> infinite loop, x > 0 -> grab until reached frame number
    device->startPreview();
    device->startLoop(10);

    device->~BMD();
    usleep(500);
    return 0;
}
